# -*- coding: utf-8 -*-
import dependency_injector.containers as containers
import dependency_injector.providers as providers

from notes.application.services.notes import SaveNotesService, GetAllNotesService

from notes.domain.services.notes import NotesDomainService

from notes.infraestructure.adapter.config.config_yaml import ConfigYaml
from notes.infraestructure.adapter.sqlalchemy import SqlAlchemyAdapter
from notes.infraestructure.sqlalchemy.notes import NotesSqlAlchemyRepository


class AdapterInjector(containers.DeclarativeContainer):
    sql_alchemy = providers.Factory(SqlAlchemyAdapter, options=ConfigYaml().get_key('db'))


class RepositoryInjector(containers.DeclarativeContainer):
    notes = providers.Singleton(NotesSqlAlchemyRepository,
                                adapter=AdapterInjector.sql_alchemy)


class DomainServicesInjector(containers.DeclarativeContainer):
    notes = providers.Singleton(NotesDomainService,
                                repository=RepositoryInjector.notes)


class AppServicesInjector(containers.DeclarativeContainer):
    get_notes = providers.Singleton(GetAllNotesService,
                                    domain_service=DomainServicesInjector.notes)

    create_notes = providers.Singleton(SaveNotesService,
                                       domain_service=DomainServicesInjector.notes)
