from flask import Flask, request, jsonify
from flask_graphql import GraphQLView
from notes.infraestructure.dependecy_injection.container import AppServicesInjector
from notes.application.graphql.schema import schema


class FlaskApi:

    def __init__(self):
        self.app = Flask(__name__)
        self.__load_routes()
        self.app.add_url_rule('/graphql', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True))

    def __load_routes(self):

        @self.app.route("/", methods=['GET'])
        def health():
            quote = {
                'message': (
                    "Para la funcionalidad principal dirigirce al servicio(flask) /notes"
                )
            }
            return response(quote, 200)

        @self.app.route("/notes", methods=['GET', 'POST'])
        def notes():
            get_notes_service = AppServicesInjector.get_notes()
            create_notes = AppServicesInjector.create_notes()
            if request.method == 'GET':
                service_response = get_notes_service.execute()
                notes = []
                for note in service_response:
                    notes.append(
                        {
                            'id': note.id,
                            'message': note.message,
                            'author': note.author,
                            'name': note.name,
                        }
                    )

                """Handles GET requests"""
                quote = {
                    'message': (
                        "Peticion Exitosa"
                    ),
                    'result': notes
                }
            elif request.method == 'POST':
                result = create_notes.execute(request.json)
                note = {
                    'id': result.id,
                    'message': result.message,
                    'author': result.author,
                    'name': result.name
                }

                """Handles GET requests"""
                quote = {
                    'message': (
                        "Peticion Exitosa"
                    ),
                    'result': note
                }
            return response(quote, 200)

        def response(resp, code):
            return jsonify(resp), code
