

class GetAllNotesService(object):

    def __init__(self, domain_service):
        self.__domain_service = domain_service

    def execute(self):
        return self.__domain_service.get_all_notes()


class SaveNotesService(object):

    def __init__(self, domain_service):
        self.__domain_service = domain_service

    def execute(self, note):
        return self.__domain_service.create(note)
