import graphene
from notes.infraestructure.dependecy_injection.container import AppServicesInjector
from notes.domain.entity.notes import Notes
get_notes_service = AppServicesInjector.get_notes()
create_note_services = AppServicesInjector.create_notes()


class Note(graphene.ObjectType):
    id = graphene.ID()
    message = graphene.String()
    name = graphene.String()
    author = graphene.String()


class createNote(graphene.Mutation):
    class Input:
        message = graphene.String()
        name = graphene.String()
        author = graphene.String()

    ok = graphene.Boolean()
    note = graphene.Field(Note)

    @classmethod
    def mutate(cls, _, args, context, info):
        requ = {'message': args.get('message'), 'name': args.get('name'), 'author': args.get('author')}
        note = create_note_services.execute(requ)
        ok = True
        return createNote(note=note, ok=ok)


class Query(graphene.ObjectType):
    note = graphene.Field(Note)
    notes = graphene.List(Note)

    def resolve_note(self, args, context, info):
        note = Notes(message="asasdasdasdasd", name="asdsadasd", author="asdasdasdasdasd")
        note.id = 12
        return note

    def resolve_notes(self, args, context, info):
        return get_notes_service.execute()


class MyMutations(graphene.ObjectType):
    create_note = createNote.Field()


schema = graphene.Schema(query=Query, mutation=MyMutations, types=[Note])
