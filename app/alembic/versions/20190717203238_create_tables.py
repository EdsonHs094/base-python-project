"""create_tables

Revision ID: 9e3c74df52ba
Revises: 
Create Date: 2019-07-17 20:32:38.566677

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9e3c74df52ba'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'notes',
        sa.Column('id', sa.Integer, primary_key=True, nullable=False, autoincrement=True),
        sa.Column('message', sa.dialects.mysql.VARCHAR(200), nullable=False),
        sa.Column('name', sa.dialects.mysql.VARCHAR(60), nullable=False),
        sa.Column('author', sa.dialects.mysql.VARCHAR(60), nullable=False),
        mysql_comment='notes table',
        mysql_default_charset='utf8',
        mysql_engine='InnoDB'
    )


def downgrade():
    op.drop_table('notes')
