from unittest import TestCase

class UnitTestProbe(TestCase):
   
    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')