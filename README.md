### Instalar Aplicacion

Generar la imagen ejecutamos el comando build
```sh
$ make build
```

Ejecutamos las migraciones
```sh
$ make migrate 
```

Levantamos el aplicativo, Se levantara en localhost:9000
```sh
$ make up
```

En la carpeta app/config crear un archivo llamado mysql.private.yml con la siguiente estructura
```sh
 db:
  connection: "mysql"
  host: "..."
  username: "..."
  password: "..."
  port: "..."
  database: "..."
  lifetime: 180

```

Para verificar entrar a localhost:9000

### Uso

Este servicio solo riene 2 rutas que son las siguientes

  - /: ruta base donde da un mensaje simple
  - /notes: metodos GET y POST para la creacion y obtencion de notas

